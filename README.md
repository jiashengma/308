# Reverse Gerrymandering
## Authors
* Jia Sheng Ma (jiasheng.ma@yahoo.com)
* Tianyi Lan
* Kaho Kui
* Luke Deiulio

## TODO: Use Cases
[x]  1. Register
[x]  2. Login
[x]  3. Edit user information
[x]  4. Go to Display/About Page
[x]  5. Select State
[x]  6. Display congressional district of the state in a map
[x]  7. Select District
[x]  8. Select year
[x]  9. Show Demographic Information
[x]  10. Show Efficiency Gap Results
[x]  11. Show statistical test measure result
[x]  14. Show Lopsided Outcomes Result
[x]  17. Calculate Compactness measure
[x]  25. View previous election results for the district
[x]  26. Show help page
[x]  27. Show credit page
[x]  28. Logout
[x]  29. Approve User Account
[x]  30. Delete User Account
[x]  32. Load Data
[x]  33. View Website Statistics

[\]  21. Validate super district
[\]  35. Upload new geopolitical data
[\]  36. Upload new demographic data

[]  12. Perform excess seat measure
[]  13. Aggregate Excess Seat Results
[]  15. Perform Consistent Advantage Test (mean median)
[]  16. Perform reliable wins test
[]  18. Compare Gerrymandering measure results
[]  19. Create Super District manually
[]  20. Create Super District using Automated methods
[]  22. Simulate super district
[x]  23. Export current work to a database
[]  24. Import saved work from a database

ADMIN (29-36)
[x]  31. Manage User permissions
[]  34. Delete/View/Save Test Results
